#! /bin/sh

BASE=/sys/class/sound

. /usr/share/debconf/confmodule
db_get auto-install/enable
auto=$RET

db_get espeakup/voice
voice=$RET
[ -n "$voice" ] || voice=en
db_get espeakup/card
card=$RET

db_stop

# take back console
exec < /dev/tty1

strip () {
	cardid=${1#$BASE/card}
	echo ${cardid%/id}
}

if lsmod | grep -q speakup_soft; then
	# Give drivers some time to detect boards :/
	echo "Starting speech synthesis, please wait while we probe your sound card(s)..."
	sleep 1
	S=1
	while true
	do
		if [ -n "$card" ]; then
			echo "$card" > /var/run/espeakup.card
		else
			IDS=$(echo $BASE/card*/id)
			if [ "$IDS" = "$BASE/card*/id" ]; then
				if [ "$S" -ge 3 ]; then
					echo "No sound card detected after $S seconds..."
				fi
				if [ "$S" -lt 20 ]; then
					# We have seen cards taking as much as 12s to get initialized...
					sleep 1
					S=$((S+1))
					continue
				else
					echo "Can not do software speech synthesis..."
					if [ "$auto" != true ]; then
						echo "Press enter to continue anyway."
						read
					fi
					break
				fi
			fi

			IDS=$(echo $BASE/card*/id)
			N=$(echo $IDS | wc -w)

			# Sleep again as much, in case more cards are to come :/
			echo "Found $N audio card(s), waiting for $S more seconds for any other card..."
			sleep $S

			. /usr/share/alsa/utils.sh
			preinit_levels all > /dev/null 2>&1
			sanify_levels all > /dev/null 2>&1

			IDS=$(echo $BASE/card*/id)
			N=$(echo $IDS | wc -w)

			echo "Found $N audio card(s)."
			echo "Found $N audio card(s):" >> /var/log/espeakup.log
			echo "$IDS" >> /var/log/espeakup.log

			if [ "$auto" = true ]; then
				# Default to card 0
				echo 0 > /var/run/espeakup.card
			else
				case $N in
				1)
					# Just one card, can not be wrong
					echo $(strip $IDS) > /var/run/espeakup.card
					;;
				*)
					# Several cards, make the user choose
					CARD=none
					while [ "$CARD" = none ]
					do
						for ID in $IDS
						do
							i=$(strip $ID)
							ALSA_CARD=$(cat /sys/class/sound/card$i/id) /usr/bin/espeakup --alsa-volume -V "$voice" >> /var/log/espeakup.log 2>&1
							while ! [ -r /var/run/espeakup.pid ]; do sleep 1; done
							pid=$(cat /var/run/espeakup.pid)
							answer=none
							echo "Please type enter to use this sound board (number $i)"
							read -t 5 answer
							kill $pid
							while [ -r /var/run/espeakup.pid ]; do sleep 1; done
							if [ "$answer" != none ]
							then
								CARD=$i
								break
							fi
						done
					done
					echo "$CARD" > /var/run/espeakup.card
					export ALSA_CARD=$(cat /sys/class/sound/card$CARD/id)
				esac
			fi
		fi

		echo "$voice" > /var/run/espeakup.voice

		# Keep espeakup running, restarting it if is ever crashes or is killed for language shift
		while true; do
			voice=$(cat /var/run/espeakup.voice)
			echo starting espeakup with card "'$ALSA_CARD'" and voice "'$voice'"
			/usr/bin/espeakup --alsa-volume -V "$voice"
			echo espeakup started
			while ! [ -r /var/run/espeakup.pid ]; do sleep 1; done
			echo espeakup running
			while true ; do
				pid=$(cat /var/run/espeakup.pid 2> /dev/null)
				if [ -z "$pid" ]; then
					echo espeakup exited, restarting it
					break
				fi
				if ! kill -0 $pid; then
					echo espeakup crashed, restarting it
					break
				fi
				sleep 1
			done
		done >> /var/log/espeakup.log 2>&1 &

		# Wait for its start before printing the first question
		while ! [ -r /var/run/espeakup.pid ]; do sleep 1; done

		# Ok, let's go!
		break
	done
fi
